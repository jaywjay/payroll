class CreateCompanies < ActiveRecord::Migration[6.0]
  def change
    create_table :companies do |t|
      t.references :user, null: false, foreign_key: true
      t.string :name
      t.string :official_name
      t.string :pin_number
      t.string :address
      t.string :email_address

      t.timestamps
    end
  end
end
