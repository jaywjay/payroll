class CreateHrEmployees < ActiveRecord::Migration[6.0]
  def change
    create_table :hr_employees do |t|
      t.string :firstname
      t.string :othernames
      t.string :email
      t.string :tel
      t.string :id_number
      t.string :nssf_number
      t.string :nhif_number
      t.string :pin_number
      t.string :address
      t.string :city
      t.string :postcode
      t.string :county
      t.string :country
      t.date :date_of_birth
      t.date :date_employed

      t.timestamps
    end
  end
end
