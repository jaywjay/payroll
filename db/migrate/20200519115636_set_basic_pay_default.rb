class SetBasicPayDefault < ActiveRecord::Migration[6.0]
  def change
    change_column_default :hr_employees, :basic_pay, from: nil, to: 0
    change_column_default :hr_employees, :house_allowance, from: nil, to: 0
  end
end
