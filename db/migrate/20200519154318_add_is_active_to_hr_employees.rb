class AddIsActiveToHrEmployees < ActiveRecord::Migration[6.0]
  def change
    add_column :hr_employees, :is_active, :int, default: 1
  end
end
