class AddBasicPayToHrEmployees < ActiveRecord::Migration[6.0]
  def change
    add_column :hr_employees, :basic_pay, :bigint
  end
end
