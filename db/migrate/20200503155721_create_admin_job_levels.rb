class CreateAdminJobLevels < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_job_levels do |t|
      t.string :job_level_name
      t.string :description

      t.timestamps
    end
  end
end
