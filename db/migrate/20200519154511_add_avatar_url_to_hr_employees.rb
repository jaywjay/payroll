class AddAvatarUrlToHrEmployees < ActiveRecord::Migration[6.0]
  def change
    add_column :hr_employees, :avatar_url, :string
  end
end
