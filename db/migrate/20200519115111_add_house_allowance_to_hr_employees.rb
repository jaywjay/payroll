class AddHouseAllowanceToHrEmployees < ActiveRecord::Migration[6.0]
  def change
    add_column :hr_employees, :house_allowance, :bigint
  end
end
