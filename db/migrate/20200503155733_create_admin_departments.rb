class CreateAdminDepartments < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_departments do |t|
      t.string :department_name
      t.string :description

      t.timestamps
    end
  end
end
