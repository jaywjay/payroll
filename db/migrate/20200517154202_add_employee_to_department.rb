class AddEmployeeToDepartment < ActiveRecord::Migration[6.0]
  def change
    add_reference :hr_employees, :admin_departments, null: false, foreign_key: true
  end
end
