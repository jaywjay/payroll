# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_19_154511) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_companies", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "company_name"
    t.string "official_name"
    t.string "pin_number"
    t.string "address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_admin_companies_on_user_id"
  end

  create_table "admin_departments", force: :cascade do |t|
    t.string "department_name"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_job_levels", force: :cascade do |t|
    t.string "job_level_name"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_positions", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.integer "deleted"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "artists", force: :cascade do |t|
    t.string "name"
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_artists_on_user_id"
  end

  create_table "companies", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "name"
    t.string "official_name"
    t.string "pin_number"
    t.string "address"
    t.string "email_address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_companies_on_user_id"
  end

  create_table "hr_employees", force: :cascade do |t|
    t.string "firstname"
    t.string "othernames"
    t.string "email"
    t.string "tel"
    t.string "id_number"
    t.string "nssf_number"
    t.string "nhif_number"
    t.string "pin_number"
    t.string "address"
    t.string "city"
    t.string "postcode"
    t.string "county"
    t.string "country"
    t.date "date_of_birth"
    t.date "date_employed"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "admin_departments_id", null: false
    t.bigint "basic_pay", default: 0
    t.bigint "house_allowance", default: 0
    t.integer "is_active", default: 1
    t.string "avatar_url"
    t.index ["admin_departments_id"], name: "index_hr_employees_on_admin_departments_id"
  end

  create_table "records", force: :cascade do |t|
    t.string "title"
    t.string "year"
    t.bigint "artist_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["artist_id"], name: "index_records_on_artist_id"
    t.index ["user_id"], name: "index_records_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "password_digest", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "admin_companies", "users"
  add_foreign_key "artists", "users"
  add_foreign_key "companies", "users"
  add_foreign_key "hr_employees", "admin_departments", column: "admin_departments_id"
  add_foreign_key "records", "artists"
  add_foreign_key "records", "users"
end
