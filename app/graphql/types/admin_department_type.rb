module Types
  class AdminDepartmentType < Types::BaseObject
    field :id, ID, null: false
    field :department_name, String, null: true

    field :employees, [Types::HrEmployeeType], null: true
  end
end
