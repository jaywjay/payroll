module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    field :admin_department, Types::AdminDepartmentType, null: false do
      argument :id, ID, required: true
    end

    def admin_department(id:)
      Admin::Department.find(id)
    end
  end
end
