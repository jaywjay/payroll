module Types
  class HrEmployeeType < Types::BaseObject
    field :id, ID, null: false
    field :firstname, String, null: true
    field :othernames, String, null: true
  end
end
