class Hr::EmployeesController < ApplicationController
  before_action :authorize_access_request!
  before_action :set_hr_employee, only: [:show, :update, :destroy]

  # GET /hr/employees
  def index
    @hr_employees = Hr::Employee.all

    render json: @hr_employees
  end

  # GET /hr/employees/1
  def show
    render json: @hr_employee
  end

  # GET /hr/search_employees
  def search
    search = params[:query]
    wildcard_search = "%#{search}%"
    @conn = ActiveRecord::Base.connection

    @results = @conn.exec_query(
      "SELECT 
      e.*
      , d.department_name
      , d.description as department_desc 
      FROM hr_employees e 
      LEFT JOIN admin_departments d ON d.id = e.admin_departments_id 
      WHERE LOWER(e.firstname) LIKE '#{wildcard_search}' 
      OR LOWER(e.othernames) LIKE '#{wildcard_search}'
      OR LOWER(d.department_name) LIKE '#{wildcard_search}'
      ")
    render json: @results
  end

  # POST /hr/employees
  def create
    @hr_employee = Hr::Employee.new(hr_employee_params)
    
    if @hr_employee.save
      render json: @hr_employee, status: :created, location: @hr_employee
    else
      render json: @hr_employee.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /hr/employees/1
  def update
    if @hr_employee.update(hr_employee_params)
      render json: @hr_employee
    else
      render json: @hr_employee.errors, status: :unprocessable_entity
    end
  end

  # DELETE /hr/employees/1
  def destroy
    @hr_employee.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hr_employee
      @hr_employee = Hr::Employee.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def hr_employee_params
      params.require(:hr_employee).permit(:admin_departments_id, :firstname, :othernames, :email, :tel, :id_number, :nssf_number, :nhif_number, :pin_number, :address, :city, :postcode, :county, :country, :date_of_birth, :date_employed, :basic_pay, :house_allowance, :is_active, :avatar_url)
    end
end
