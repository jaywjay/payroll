class Admin::DepartmentsController < ApplicationController
  before_action :authorize_access_request!
  before_action :set_admin_department, only: [:show, :update, :destroy]

  # GET /admin/departments
  def index
    @admin_departments = Admin::Department.all

    render json: @admin_departments
  end

  # GET /admin/departments/1
  def show
    render json: @admin_department
  end

  def employees
    @employees = Hr::Employees.all

    render json: @employees
  end

  def search
    @admin_departments = Admin::Department.search_by_term(params[:query])
    render json: @admin_departments
  end

  # POST /admin/departments
  def create
    @admin_department = Admin::Department.new(admin_department_params)

    if @admin_department.save
      render json: @admin_department, status: :created, location: @admin_department
    else
      render json: @admin_department.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /admin/departments/1
  def update
    if @admin_department.update(admin_department_params)
      render json: @admin_department
    else
      render json: @admin_department.errors, status: :unprocessable_entity
    end
  end

  # DELETE /admin/departments/1
  def destroy
    @admin_department.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_department
      @admin_department = Admin::Department.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def admin_department_params
      params.require(:admin_department).permit(:department_name, :description)
    end
end
