class Admin::JobLevelsController < ApplicationController
  before_action :authorize_access_request!
  before_action :set_admin_job_level, only: [:show, :update, :destroy]

  # GET /admin/job_levels
  def index
    @admin_job_levels = Admin::JobLevel.all

    render json: @admin_job_levels
  end

  # GET /admin/job_levels/1
  def show
    render json: @admin_job_level
  end

  # POST /admin/job_levels
  def create
    @admin_job_level = Admin::JobLevel.new(admin_job_level_params)

    if @admin_job_level.save
      render json: @admin_job_level, status: :created, location: @admin_job_level
    else
      render json: @admin_job_level.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /admin/job_levels/1
  def update
    if @admin_job_level.update(admin_job_level_params)
      render json: @admin_job_level
    else
      render json: @admin_job_level.errors, status: :unprocessable_entity
    end
  end

  # DELETE /admin/job_levels/1
  def destroy
    @admin_job_level.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_job_level
      @admin_job_level = Admin::JobLevel.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def admin_job_level_params
      params.require(:admin_job_level).permit(:job_level_name, :description)
    end
end
