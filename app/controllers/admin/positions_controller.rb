class Admin::PositionsController < ApplicationController
  before_action :authorize_access_request!
  before_action :set_admin_position, only: [:show, :update, :destroy]

  # GET /admin/positions
  def index
    @admin_positions = Admin::Position.all

    render json: @admin_positions
  end

  # GET /admin/positions/1
  def show
    render json: @admin_position
  end

  # POST /admin/positions
  def create
    @admin_position = Admin::Position.new(admin_position_params)

    if @admin_position.save
      render json: @admin_position, status: :created, location: @admin_position
    else
      render json: @admin_position.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /admin/positions/1
  def update
    if @admin_position.update(admin_position_params)
      render json: @admin_position
    else
      render json: @admin_position.errors, status: :unprocessable_entity
    end
  end

  # DELETE /admin/positions/1
  def destroy
    @admin_position.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_position
      @admin_position = Admin::Position.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def admin_position_params
      params.require(:admin_position).permit(:name, :description, :deleted)
    end
end
