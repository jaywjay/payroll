class HomeController < ApplicationController
  def index
    @employees = Company.all
    render json: @employees
  end
end