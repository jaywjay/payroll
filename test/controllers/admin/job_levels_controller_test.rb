require 'test_helper'

class Admin::JobLevelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_job_level = admin_job_levels(:one)
  end

  test "should get index" do
    get admin_job_levels_url, as: :json
    assert_response :success
  end

  test "should create admin_job_level" do
    assert_difference('Admin::JobLevel.count') do
      post admin_job_levels_url, params: { admin_job_level: { description: @admin_job_level.description, job_level_name: @admin_job_level.job_level_name } }, as: :json
    end

    assert_response 201
  end

  test "should show admin_job_level" do
    get admin_job_level_url(@admin_job_level), as: :json
    assert_response :success
  end

  test "should update admin_job_level" do
    patch admin_job_level_url(@admin_job_level), params: { admin_job_level: { description: @admin_job_level.description, job_level_name: @admin_job_level.job_level_name } }, as: :json
    assert_response 200
  end

  test "should destroy admin_job_level" do
    assert_difference('Admin::JobLevel.count', -1) do
      delete admin_job_level_url(@admin_job_level), as: :json
    end

    assert_response 204
  end
end
