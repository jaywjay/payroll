require 'test_helper'

class Admin::PositionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_position = admin_positions(:one)
  end

  test "should get index" do
    get admin_positions_url, as: :json
    assert_response :success
  end

  test "should create admin_position" do
    assert_difference('Admin::Position.count') do
      post admin_positions_url, params: { admin_position: { deleted: @admin_position.deleted, description: @admin_position.description, name: @admin_position.name } }, as: :json
    end

    assert_response 201
  end

  test "should show admin_position" do
    get admin_position_url(@admin_position), as: :json
    assert_response :success
  end

  test "should update admin_position" do
    patch admin_position_url(@admin_position), params: { admin_position: { deleted: @admin_position.deleted, description: @admin_position.description, name: @admin_position.name } }, as: :json
    assert_response 200
  end

  test "should destroy admin_position" do
    assert_difference('Admin::Position.count', -1) do
      delete admin_position_url(@admin_position), as: :json
    end

    assert_response 204
  end
end
