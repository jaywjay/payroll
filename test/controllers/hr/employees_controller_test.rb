require 'test_helper'

class Hr::EmployeesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @hr_employee = hr_employees(:one)
  end

  test "should get index" do
    get hr_employees_url, as: :json
    assert_response :success
  end

  test "should create hr_employee" do
    assert_difference('Hr::Employee.count') do
      post hr_employees_url, params: { hr_employee: { address: @hr_employee.address, city: @hr_employee.city, country: @hr_employee.country, county: @hr_employee.county, date_employed: @hr_employee.date_employed, date_of_birth: @hr_employee.date_of_birth, email: @hr_employee.email, firstname: @hr_employee.firstname, id_number: @hr_employee.id_number, nhif_number: @hr_employee.nhif_number, nssf_number: @hr_employee.nssf_number, othernames: @hr_employee.othernames, pin_number: @hr_employee.pin_number, postcode: @hr_employee.postcode, tel: @hr_employee.tel } }, as: :json
    end

    assert_response 201
  end

  test "should show hr_employee" do
    get hr_employee_url(@hr_employee), as: :json
    assert_response :success
  end

  test "should update hr_employee" do
    patch hr_employee_url(@hr_employee), params: { hr_employee: { address: @hr_employee.address, city: @hr_employee.city, country: @hr_employee.country, county: @hr_employee.county, date_employed: @hr_employee.date_employed, date_of_birth: @hr_employee.date_of_birth, email: @hr_employee.email, firstname: @hr_employee.firstname, id_number: @hr_employee.id_number, nhif_number: @hr_employee.nhif_number, nssf_number: @hr_employee.nssf_number, othernames: @hr_employee.othernames, pin_number: @hr_employee.pin_number, postcode: @hr_employee.postcode, tel: @hr_employee.tel } }, as: :json
    assert_response 200
  end

  test "should destroy hr_employee" do
    assert_difference('Hr::Employee.count', -1) do
      delete hr_employee_url(@hr_employee), as: :json
    end

    assert_response 204
  end
end
