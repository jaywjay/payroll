Rails.application.routes.draw do
  
  post "/graphql", to: "graphql#execute"

  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "graphql#execute"
  end

  resources :companies
  namespace :admin do
    resources :positions
  end
  namespace :admin do
    resources :departments
  end
  namespace :admin do
    resources :job_levels
  end
  namespace :hr do
    resources :employees
  end
  root to: "home#index"


  post "refresh", controller: :refresh, action: :create
  post "signin", controller: :signin, action: :create
  post "signup", controller: :signup, action: :create
  delete "signin", controller: :signin, action: :destroy
  get '/admin/departments/search' => 'admin/departments#search'
  get '/hr/search_employees' => 'hr/employees#search'

end