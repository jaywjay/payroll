import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Login from '@/components/auth/Login.vue'
import Signup from '@/components/auth/Signup.vue'
import Employees from '@/components/hr/Employees.vue'
import EmployeeEditor from '@/components/hr/EmployeeEditor.vue'
import Departments from '@/components/admin/Departments.vue'

Vue.use(VueRouter)

  const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup
  },
  {
    path: '/employees',
    name: 'Employees',
    component: Employees
  },
  {
    path: '/employees/:id',
    name: 'EmployeeEditor',
    component: EmployeeEditor
  },
  {
    path: '/admin/departments',
    name: 'Departments',
    component: Departments
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
